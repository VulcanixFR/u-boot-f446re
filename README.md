# About this project

Just trying to play with the STM32F446RE Nucleo64 dev board.

## Project status

### 2024/07/02

The U-boot manages to initialize up to the USART initialization.
The device fails to configure the clock source of the USART2 
peripheral. The clock is missing from the device driver structure.

The problem comes from the device tree. I tried using clk-hsi 
from the ST driver, instead of the clk-hse, since the nucleo 
board uses the HDI clock with the internal PLL. 
Further investigation on the clock driver should be conducted.

The corresponding .config file is named `config_dev_f446__usart_fail_hsi_clock`.